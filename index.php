<?PHP

//init

$serverCount = 0;
$serverInfo = array();

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Lunch And LAN</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<meta charset="utf-8">
	</head>
	<style>
		.rotate
		{
			-moz-transition: all 0.25s linear;
			-webkit-transition: all 0.25s linear;
			transition: all 0.25s linear;
		}

		.rotate.down
		{
			-ms-transform: rotate(180deg);
			-moz-transform: rotate(180deg);
			-webkit-transform: rotate(180deg);
			transform: rotate(180deg);
		}
		.roundHeader
		{
			color: #FFFFFF;
    		border-radius: 3px;
    		padding: 0px;
			background-color: #5bc0de;
			border-color: #46b8da;
			cursor: pointer;
		}
		.glyphicon
		{
    		font-size: 20px;
		}
		.loading
		{
			position: absolute;
			z-index: 2;
			left: 50%;
			margin-left: -16px;
			top: 0;
			visibility: hidden;
			border: 4px solid transparent;
			border-radius: 50%;
			border-top: 4px solid #000000;
			border-bottom: 4px solid #000000;
			width: 32px;
			height: 32px;
			-webkit-animation: spin 1100ms linear infinite; /* Safari */
			animation: spin 1100ms linear infinite;
		}
		/* Safari */
		@-webkit-keyframes spin
		{
 			0% { -webkit-transform: rotate(0deg); }
			100% { -webkit-transform: rotate(360deg); }
		}
		@keyframes spin
		{
			0% { transform: rotate(0deg); }
			100% { transform: rotate(360deg); }
		}
	</style>
<body style="background-color:#C0C0C0;" onload="refresh()">

<div class="container">
	<h1 class="text-center"><i>Lunch and <b>LAN</b></i></h1>
	<p>Get the game... <a href="../q1.zip" class="btn btn-info btn-sm" role="button" target="_blank">Download Quake1</a>
</div>

<br>
<br>

<div class="container">
	<p class="text-center roundHeader" data-toggle="collapse" data-target="#list" onclick="$('#expBtn').toggleClass('down');">
		<span class=" align-middle" style="font-size: 18pt;">&nbsp; &nbsp; <i>Server List</i></span>
		<span class="pull-right" style="padding-right:8px; padding-top:6px;">
			<span id="expBtn" class="glyphicon glyphicon-chevron-down rotate down"></span>
		</span>
	</p>
	<div id="list" class="container-fluid collapse in">
		<br>
		<div class="container-fluid text-center">
			<div style="position: relative; height: 48px;">
				<div id="loadInd" class="loading"></div>
				<button id="refreshBtn" class="btn btn-warning btn-sm" style="color:#000000; z-index: 1;" onclick="refresh()">Refresh</button>
			</div>
		</div>
		<br>
		<div class="table-responsive">
		  	<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Server Name</th>
						<th>IP Address</th>
						<th>Map Name</th>
						<th>Active Clients</th>
						<th>Max Clients</th>
					</tr>
				</thead>
				<tbody id="slistTable">

<?php
if($serverCount > 0)
{
	for($i = 0; $i < $serverCount; $i++)
	{
?>
					<tr style="background-color:#A1A1A1">
						<th scope="row"><?php echo ($i+1); ?></th>
						<td><?php echo $serverInfo[$i]['host']; ?></td>
						<td><?php echo $serverInfo[$i]['ip']; ?></td>
						<td><?php echo $serverInfo[$i]['map']; ?></td>
						<td><?php echo $serverInfo[$i]['actCl']; ?></td>
						<td><?php echo $serverInfo[$i]['maxCl']; ?></td>
					</tr>
<?php
	}
}
else
{
?>
					<tr style="background-color:#A1A1A1">
						<th scope="row"></th>
						<td><span style="color:#FFFFFF;"><b><i>No Servers Found</i></b></span></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
<?php
}
?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
var noServerNode = document.getElementById("slistTable").children[0].cloneNode(true);

function refresh()
{
	var xhttp = new XMLHttpRequest();
	document.getElementById("loadInd").style.visibility = "visible";
	document.getElementById("refreshBtn").style.visibility = "hidden";
	xhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 200)
		{
			parseResponse(this.responseText);
			document.getElementById("loadInd").style.visibility = "hidden";
			document.getElementById("refreshBtn").style.visibility = "visible";
		}
	};
	xhttp.open("GET", "slist.php", true);
	xhttp.send();
}

function parseResponse(str)
{
	var serverCount = 0;
	if(str.length > 1)
	{
		var rows = str.split("~");					// ~ divides server rows
		serverCount = rows.shift();
	}
	else
	{
		var rows = new Array();
	}

	var rowCt = rows.length;
	var serverInfo = new Array();

    for(var i = 0; i < rowCt; i++)
	{
		serverInfo[i] = rows[i].split("`");			// ` divides fields
	}

	if(serverCount == serverInfo.length)
	{
		slist(serverInfo);
	}
	else
	{
		alert('serverCount does not match');
	}
}

function slist(serverInfo)
{
	var serverCount = serverInfo.length;
	var tbBody = document.getElementById("slistTable");
    var rowClone = tbBody.children[0].cloneNode(true);
    var rowCt = tbBody.childElementCount;

    colCt = rowClone.childElementCount;
    for(var i = 0; i < colCt; i++)
    {
    	rowClone.children[i].innerHTML = "";
    }

    for(var i = 0; i < rowCt; i++)
    {
    	tbBody.removeChild(tbBody.children[0]);
    }

	if(serverCount > 0)
	{
		for(var i = 0; i < serverCount; i++)
	    {
	    	cloneCpy = rowClone.cloneNode(true);
	        cloneCpy.children[0].innerHTML = (i+1);
	        cloneCpy.children[1].innerHTML = serverInfo[i][1];
	        cloneCpy.children[2].innerHTML = serverInfo[i][0];
	        cloneCpy.children[3].innerHTML = serverInfo[i][2];
	        cloneCpy.children[4].innerHTML = serverInfo[i][3];
	        cloneCpy.children[5].innerHTML = serverInfo[i][4];

	    	tbBody.appendChild(cloneCpy);
	    }
	}
	else
	{
		tbBody.appendChild(noServerNode);
	}
}
</script>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
<?php
//end
?>
