<?PHP

//init

//QUAKE Message								//See net_defs.h and net_dgrm.c
$sendRaw['name'] = "8000";
$sendRaw['flags'] = "000C";					//NAK,EOM maybe?
$sendRaw['sequence'] = "02";				//CCREQ_SERVER_INFO
$sendRaw['data'] = "5155414B450003";		//"QUAKE" Protocol 03
$packet = pack("H*", implode($sendRaw));	//Condense into binary data

$bcastAddress = "255.255.255.255";			//Broadcast request message to this address
$qPort = 26000;								//Broadcast on this port

$maxReplies = 10;							//Max number of replies to read
$serverCount = 0;							//Number of servers found
$serverInfo = array();						//Two dimentional array of server info
$dataStr = array();
$retries = 2;								//Poll retries after unsuccessful read
$retryRst = $retries;

//Create UDP Socket
$sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
socket_set_option($sock, SOL_SOCKET, SO_RCVTIMEO, array("sec"=>5,"usec"=>0));
socket_set_option($sock, SOL_SOCKET, SO_BROADCAST, 1);

//Broadcast server info request packet
socket_sendto($sock, $packet, strlen($packet), 0, $bcastAddress, $qPort);

//Receive server info
while(true)
{
	$dataRaw = socket_read($sock, 1024);			//Read binary packet data

	if(strlen($dataRaw) == 0)						//No Data
	{
		if($retries == 1)
		{
			break;
		}
		else
		{
			$retries--;
		}
	}
	elseif ($serverCount == $maxReplies)			//Only gather $maxReplies of packets
	{
		break;
	}
	else
	{
		$dataStr[$serverCount] = implode(unpack("H*", $dataRaw));		//To string of hex
		$serverCount++;
		$retries = $retryRst;											//Successful read, reset retries
	}
}

//Parse received data
for($i = 0; $i < $serverCount; $i++)
{
	if($dataStr[$i] != "" && (strlen($dataStr[$i]) % 2) == 0)	//String is not empty and string has even number of chars
	{
		$dataArr = str_split($dataStr[$i], 2);					//Split to an array

		if(sizeof($dataArr) > 4 && $dataArr[4] == "83")			//If reply is CCREP_SERVER_INFO
		{
			$ipOfs = 5;											//IP:Port offset
			$hostOfs = arrStrOfs($dataArr, $ipOfs);				//Host name offset
			$mapOfs = arrStrOfs($dataArr, $hostOfs);			//Map name offset
			$actClOfs = arrStrOfs($dataArr, $mapOfs);			//Active clients offset
			$maxClOfs = $actClOfs + 1;							//Max clients offset

			$serverInfo[$i] = array('ip' => '', 'host' => '', 'map' => '', 'actCl' => '', 'maxCl' => '');
			$serverInfo[$i]['ip'] = pack("H*", implode(array_slice($dataArr, $ipOfs, ($hostOfs - $ipOfs))));
			$serverInfo[$i]['host'] = pack("H*", implode(array_slice($dataArr, $hostOfs, ($mapOfs - $hostOfs))));
			$serverInfo[$i]['map'] = pack("H*", implode(array_slice($dataArr, $mapOfs, ($actClOfs - $mapOfs))));
			$serverInfo[$i]['actCl'] = hexdec(implode(array_slice($dataArr, $actClOfs, 1)));
			$serverInfo[$i]['maxCl'] = hexdec(implode(array_slice($dataArr, $maxClOfs, 1)));
		}
	}
}

//Step through to find delimiter from start offset to eol
function arrStrOfs($arr, $start)
{
	$offset = $start;
	$len = sizeof($arr);
	for($x = $start; $x < $len; $x++)
	{
		if($arr[$x] == "00")									//Strings end with null (0x00)
		{
			$offset = $x + 1;
			break;
		}
	}
	return $offset;
}

//first four bytes are trash
//fifth byte = 83 (CCREP_SERVER_INFO)
//ip until 0x00 null
//host name until 0x00 null
//map name until 0x00 null
//active connections 1 byte
//max clients 1 byte
//NET_PROTOCOL_VERSION = 03
//eof

socket_close($sock);

$returnStr = "";
$returnStr .= $serverCount;
for($i = 0; $i < $serverCount; $i++)
{
	$returnStr .= "~";
	$returnStr .= implode("`", $serverInfo[$i]);
}

//exit($returnStr);
echo $returnStr;

//end
?>
